### Opswat Akadémia - CMake

* minden branch a hozzá tartozó feladatot tartalmazza

#### feladat1:

##### add simplified build script (only for illustration)

##### add basic cmake file:

Egy darab CMakeLists.txt file mely előállít egy example nevű binárist (futtatható állományt) az alábbi forrásfájlok segítségével:
* src/main.cpp
* src/welcome.cpp
* src/logger/logger.cpp

A CMakeLists.txt file magyarázata:

* `PROJECT(CMake-example)`\
    beállítja a project nevét 'CMake-example'-re\
    [CMake: project](https://cmake.org/cmake/help/latest/command/project.html)

* `SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fPIC")`\
    beállítja az '-fPIC' flaget

* `SET(CMAKE_CXX_STANDARD 14)`\
    beállítja a C++14-es szabvány használatát\
    [CMake: CMAKE_CXX_STANDARD](https://cmake.org/cmake/help/v3.15/variable/CMAKE_CXX_STANDARD.html)

* `include_directories(src)`\
    A fordító a megadott könyvtár(ak)ban is kerese header fájlokat az előfeldolgozás során\
    [CMake: include_directories](https://cmake.org/cmake/help/v3.0/command/include_directories.html)\
    [g++: -I dir](https://linux.die.net/man/1/g++)
    [g++: Options for Directory Search rész](http://man7.org/linux/man-pages/man1/g++.1.html)

* `add_executable(example src/main.cpp src/welcome.cpp src/logger/logger.cpp)`\
    létrehoz egy `example` nevű futtatható állományt a megadott forrásfájlokból\
    [CMake: add_executable](https://cmake.org/cmake/help/latest/command/add_executable.html)
